def namein(tag, names):
    "Check whether the name part of a infra-name-count tag is in names."
    return tag.split('-')[1] in names

class TestModule(object):
    def tests(self):
        return {
            'namein': namein
        }

# eof
